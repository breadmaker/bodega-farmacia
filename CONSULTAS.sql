DROP TABLE IF EXISTS bodega;

DROP TABLE IF EXISTS categoria_producto;

DROP TABLE IF EXISTS documento;

DROP TABLE IF EXISTS documento_producto;

DROP TABLE IF EXISTS principio_activo;

DROP TABLE IF EXISTS producto;

DROP TABLE IF EXISTS producto_bodega;

DROP TABLE IF EXISTS usuario;

CREATE TABLE bodega
(
   id_bodega            INT NOT NULL,
   nombre_bodega        VARCHAR(32) NOT NULL,
   PRIMARY KEY (id_bodega)
);

CREATE TABLE categoria_producto
(
   id_categoria_producto INT NOT NULL,
   nombre_categoria_producto VARCHAR(64) NOT NULL,
   PRIMARY KEY (id_categoria_producto)
);

CREATE TABLE documento
(
   id_documento         INT NOT NULL,
   tipo_documento       VARCHAR(16) NOT NULL,
   fecha_documento      DATETIME NOT NULL,
   id_bodega_origen     INT,
   id_bodega_destino    INT,
   PRIMARY KEY (id_documento)
);

CREATE TABLE documento_producto
(
   cod_producto         INT NOT NULL,
   id_documento         INT NOT NULL,
   cantidad             INT NOT NULL,
   PRIMARY KEY (cod_producto, id_documento)
);

CREATE TABLE principio_activo
(
   id_principio_activo  INT NOT NULL,
   nombre_principio_activo VARCHAR(64) NOT NULL,
   PRIMARY KEY (id_principio_activo)
);

CREATE TABLE producto
(
   cod_producto         INT NOT NULL,
   id_principio_activo  INT,
   id_categoria_producto INT NOT NULL,
   nombre_producto      VARCHAR(64) NOT NULL,
   es_critico_producto  BOOLEAN NOT NULL,
   PRIMARY KEY (cod_producto)
);

CREATE TABLE producto_bodega
(
   cod_producto         INT NOT NULL,
   id_bodega            INT NOT NULL,
   stock_producto_bodega INT NOT NULL,
   stock_minimo_producto_bodega INT NOT NULL,
   PRIMARY KEY (cod_producto, id_bodega)
);

CREATE TABLE usuario
(
   id_usuario           INT NOT NULL,
   id_bodega            INT,
   nombre_usuario       VARCHAR(64) NOT NULL,
   nickname_usuario     VARCHAR(32) NOT NULL,
   password_usuario     VARCHAR(32) NOT NULL,
   es_administrador_usuario BOOLEAN NOT NULL,
   PRIMARY KEY (id_usuario)
);

ALTER TABLE documento_producto ADD CONSTRAINT fk_documento_producto FOREIGN KEY (cod_producto)
      REFERENCES producto (cod_producto) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE documento_producto ADD CONSTRAINT fk_documento_producto2 FOREIGN KEY (id_documento)
      REFERENCES documento (id_documento) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE producto ADD CONSTRAINT fk_producto_categoria FOREIGN KEY (id_categoria_producto)
      REFERENCES categoria_producto (id_categoria_producto) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE producto ADD CONSTRAINT fk_producto_principioactivo FOREIGN KEY (id_principio_activo)
      REFERENCES principio_activo (id_principio_activo) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE producto_bodega ADD CONSTRAINT fk_producto_bodega FOREIGN KEY (cod_producto)
      REFERENCES producto (cod_producto) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE producto_bodega ADD CONSTRAINT fk_producto_bodega2 FOREIGN KEY (id_bodega)
      REFERENCES bodega (id_bodega) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE usuario ADD CONSTRAINT fk_usuario_bodega FOREIGN KEY (id_bodega)
      REFERENCES bodega (id_bodega) ON DELETE RESTRICT ON UPDATE RESTRICT;

INSERT INTO bodega VALUES(1,'Bodega de Sucursal Norte');
INSERT INTO bodega VALUES(2,'Bodega de Sucursal Sur');
INSERT INTO bodega VALUES(3,'Bodega Central');
INSERT INTO usuario VALUES(1,1,'Bodeguero Norte','bode1',MD5('123456'),FALSE);
INSERT INTO usuario VALUES(2,2,'Bodeguero Sur','bode2',MD5('123456'),FALSE);
INSERT INTO usuario VALUES(3,NULL,'Supervisor','super',MD5('asdfgh'),TRUE);
INSERT INTO principio_activo VALUES(1,'Ácido Acetilsalicílico');
INSERT INTO principio_activo VALUES(2,'Ibuprofeno');
INSERT INTO principio_activo VALUES(3,'Sildenafilo');
INSERT INTO principio_activo VALUES(4,'Ácido Ascórbico');
INSERT INTO principio_activo VALUES(5,'Oxígeno');
INSERT INTO categoria_producto VALUES (1,'Medicamento');
INSERT INTO categoria_producto VALUES (2,'Aseo personal');
INSERT INTO categoria_producto VALUES (3,'Belleza');
INSERT INTO categoria_producto VALUES (4,'Bebé');
INSERT INTO categoria_producto VALUES (5,'Wellness');
INSERT INTO categoria_producto VALUES (6,'Cuidado Personal');
INSERT INTO producto VALUES(1,3,1,'VIAGRA Comp. recub. con película 100 mg.',FALSE);
INSERT INTO producto VALUES(2,1,1,'ASPIRINA Comp. 500 mg',FALSE);
INSERT INTO producto VALUES(3,5,1,'CONOXIA Gas criogénico medicinal 99,5% v/v en tanque fijo',TRUE);
INSERT INTO producto VALUES(4,2,1,'ARTICALM Gel 50 mg/g',FALSE);
INSERT INTO producto VALUES(5,4,1,'CAPICAPS Cáps.',FALSE);
INSERT INTO producto VALUES(6,NULL,4,'PAÑALES Premium Care 36 unid.',TRUE);
INSERT INTO producto VALUES(7,NULL,2,'SHAMPOO control caspa con acción inmediata',FALSE);
INSERT INTO producto VALUES(8,NULL,3,'CREMA Anti Edad Q10 Noche',FALSE);
INSERT INTO producto VALUES(9,NULL,5,'PROPÓLEO en spray',FALSE);
INSERT INTO producto VALUES(10,NULL,6,'SPRAY Desodorante Para Pies',FALSE);
INSERT INTO producto_bodega VALUES(1,1,100,20);
INSERT INTO producto_bodega VALUES(2,1,100,50);
INSERT INTO producto_bodega VALUES(3,1,5,2);
INSERT INTO producto_bodega VALUES(4,1,50,10);
INSERT INTO producto_bodega VALUES(5,1,100,20);
INSERT INTO producto_bodega VALUES(6,1,1000,100);
INSERT INTO producto_bodega VALUES(7,1,500,50);
INSERT INTO producto_bodega VALUES(8,1,100,20);
INSERT INTO producto_bodega VALUES(9,1,200,20);
INSERT INTO producto_bodega VALUES(10,1,1000,100);
INSERT INTO producto_bodega VALUES(1,2,100,20);
INSERT INTO producto_bodega VALUES(2,2,100,50);
INSERT INTO producto_bodega VALUES(3,2,5,2);
INSERT INTO producto_bodega VALUES(4,2,50,10);
INSERT INTO producto_bodega VALUES(5,2,100,20);
INSERT INTO producto_bodega VALUES(6,2,1000,100);
INSERT INTO producto_bodega VALUES(7,2,500,50);
INSERT INTO producto_bodega VALUES(8,2,100,20);
INSERT INTO producto_bodega VALUES(9,2,200,20);
INSERT INTO producto_bodega VALUES(10,2,1000,100);
INSERT INTO producto_bodega VALUES(1,3,100,20);
INSERT INTO producto_bodega VALUES(2,3,100,50);
INSERT INTO producto_bodega VALUES(3,3,5,2);
INSERT INTO producto_bodega VALUES(4,3,50,10);
INSERT INTO producto_bodega VALUES(5,3,100,20);
INSERT INTO producto_bodega VALUES(6,3,1000,100);
INSERT INTO producto_bodega VALUES(7,3,500,50);
INSERT INTO producto_bodega VALUES(8,3,100,20);
INSERT INTO producto_bodega VALUES(9,3,200,20);
INSERT INTO producto_bodega VALUES(10,3,1000,100);
