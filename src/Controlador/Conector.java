/*
 * Copyright (C) 2019 Felipe Peñailillo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package Controlador;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author Felipe Peñailillo
 */
public class Conector {

    private static Connection conn;
    private static final String DRIVER = "com.mysql.jdbc.Driver";
    private static final String USER = "root";
    private static final String PASSWORD = "root2018";
    private static final String URL = "jdbc:mysql://localhost:3306/bodega_farmacia";

    public Conector() {
        conn = null;
        String url = URL;
        try {
            Class.forName(DRIVER);
            conn = DriverManager.getConnection(url, USER, PASSWORD);
            if (conn != null) {
                System.out.println("Conexión Establecida...");
            }
        } catch (ClassNotFoundException | SQLException e) {
            System.out.println("Error al intentar conectar: " + e);
        }

    }

    public Connection getConnection() {
        return conn;
    }

    public void desconectar() {
        conn = null;
        if (conn == null) {
            System.out.println("Conexión Terminada...");
        }
    }
}
