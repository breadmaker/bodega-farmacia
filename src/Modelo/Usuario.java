/*
 * Copyright (C) 2019 Felipe Peñailillo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package Modelo;

/**
 *
 * @author Felipe Peñailillo
 */
public class Usuario {
    
    private final String username;
    private final String nickname;
    private final int id_bodega;
    private final boolean admin;

    public Usuario(String username, String nickname, int id_bodega, boolean admin) {
        this.username = username;
        this.nickname = nickname;
        this.id_bodega = id_bodega;
        this.admin = admin;
    }

    public String getUsername() {
        return username;
    }

    public String getNickname() {
        return nickname;
    }

    public int getId_bodega() {
        return id_bodega;
    }

    public boolean isAdmin() {
        return admin;
    }
}
